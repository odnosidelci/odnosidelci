from django.contrib.auth.models import User
from userprofiles.models import UserProfile
from people import PERS_m, PERS_f
from imprisonment.models import Place
from random import choice

pp = Place.objects.all()

i = 0
for p in PERS_f:
    ok = False
    try:
        u = User.objects.get(username=p[8])
        print "already exists: <" + p[8] + ">"
    except:
        ok = True
    i = i + 1
    if ok:
        try:
            u = User.objects.create_user(p[8], p[8] + "@odnosidelci.com", "dlinniyparol" + str(i))
            u.first_name = p[2]
            u.last_name = p[1]
            u.save()
            r = u.profile
            r.birth_date = p[4]
            r.second_name = p[3]
            r.sex = 'f'
            r.is_fake = True
            r.place = choice(pp)
            r.region = r.place.region
            r.state = r.region.state

            r.save()
        except:
            pass

for p in PERS_m:
    ok = False
    try:
        u = User.objects.get(username=p[8])
    except:
        ok = True
    i = i + 1
    if ok:
        try:
            u = User.objects.create_user(p[8], p[8] + "@odnosidelci.com", "dlinniyparol" + str(i))
            u.first_name = p[2]
            u.last_name = p[1]
            r = u.profile
            r.birth_date = p[4]
            r.second_name = p[3]
            r.sex = 'm'
            r.is_fake = True
            r.place = choice(pp)
            r.region = r.place.region
            r.state = r.region.state
            u.save()
            r.save()
        except:
            pass
        


