from django.conf.urls.defaults import patterns, include, url
from imprisonment.views import *

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       # Account management
                       url(r'^save/$', imprisonment_save_page),
                       url(r'^delete/(\d+)/$', imprisonment_delete),
                       url(r'^odnosid/(\d+)/$', imprisonment_odnosid),
                       url(r'^user/(\w+)/$', user_page),
)
