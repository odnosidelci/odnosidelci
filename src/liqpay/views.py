# -*- coding: utf-8 -*-

import json
import base64
from xml.dom.minidom import parseString

from django.http import HttpResponse
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

from liqpay.models import LiqPayIn


@csrf_exempt
def generate_send_vip(request, vip_to):
    """ 
    """
    data = None
    try:
        data = request.body
    except:
        data = request.raw_post_data

    data_json = json.loads(data)
    amount = 10
    liqpayin = None

    user_from = request.user
    vip_to = get_object_or_404(User, username=vip_to)

    try:
        liqpayin = LiqPayIn.objects.get(id=data_json["id"])
    except:
        liqpayin = LiqPayIn()
        liqpayin.payment_from = user_from
        liqpayin.vip_account_to = vip_to

    if (liqpayin.status == 0):
        liqpayin.amount = amount
        liqpayin.save()
        liqpayin.generate_form_fields()
        result = '{"operation_xml":"%s","signature":"%s","id":%d}' % (liqpayin.operation_xml,
                                                                      liqpayin.signature,
                                                                      liqpayin.id)
        return HttpResponse(result)


@csrf_exempt
def pay_in(request):
    """ LiqPAY payment 
    """
    # from django.core.mail import send_mail

    operation_xml = base64.b64decode(request.POST["operation_xml"])
    signature = base64.b64decode(request.POST["signature"])
    xml = parseString(operation_xml)

    order_id = xml.getElementsByTagName('order_id')[0].childNodes[0].nodeValue
    liqpayin = LiqPayIn.objects.get(id=order_id)
    liqpayin.request_in_xml = operation_xml

    status = xml.getElementsByTagName('status')[0].childNodes[0].nodeValue

    if status == 'success':
        liqpayin.status = 400

    sender = xml.getElementsByTagName('sender_phone')[0].childNodes[0].nodeValue
    liqpayin.sender = sender

    liqpayin.save()

    if liqpayin.vip_account_to:
        up = liqpayin.vip_account_to.get_profile()
        up.is_vip = True
        up.save()

    return HttpResponse("ok")

