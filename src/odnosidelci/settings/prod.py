from ConfigParser import RawConfigParser

from odnosidelci.settings.common import *

DEBUG = False
TEMPLATE_DEBUG = DEBUG

CONFIG_FILE = '/etc/odnosidelci/settings.ini'

CRON_TASK_LOG_FILENAME = '/srv/www/odnosidelci.com/log/test.log'

config = RawConfigParser()
config.read(CONFIG_FILE)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config.get('database', 'DATABASE_NAME'),
        'USER': config.get('database', 'DATABASE_USER'),
        'PASSWORD': config.get('database', 'DATABASE_PASSWORD'),
        'HOST': config.get('database', 'DATABASE_HOST'),
        'PORT': config.get('database', 'DATABASE_PORT'),
    },
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_DIR, '../../whoosh_index'),
    },
}
