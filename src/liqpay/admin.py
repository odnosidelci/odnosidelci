# -*- coding: utf-8 -*-

from django.contrib import admin
from liqpay.models import LiqPayIn


class LiqPayInAdmin(admin.ModelAdmin):
    list_display = ('payment_from','pk', 'vip_account_to','present_to')

admin.site.register(LiqPayIn, LiqPayInAdmin)
