from django.conf.urls.defaults import patterns, url, include

urlpatterns = patterns('',
                       url(r'^generate_send_vip/(\w+)/$', 'liqpay.views.generate_send_vip'),
                       url(r'^pay_in/$', 'liqpay.views.pay_in'),
)