# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.template.loader import get_template
from django.template import Context
from django.conf import settings
from smart_selects.db_fields import ChainedForeignKey

# Create your models here.



class State(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = "Страна"


class Region(models.Model):
    state = models.ForeignKey(State)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = "Регион"


class Place(models.Model):
    region = models.ForeignKey(Region)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = "Населенный пункт"


# стр.62 Django "Разаработка web-приложений на python"
ROUTINE_CHOICES = enumerate((
    u'Общий режим',
    u'Усиленный режим',
    u'Строгий режим',
    u'Особый режим',
    u'Колония-поселение',
    u'Воспитательная для несовершеннолетних',
    u'Лечебно-исправительное учреждение',
    u'Тюрьма',
    u'Исправительная колония',
    u'Следственный изолятор',
    u'Объединение исправительных колоний',
))

TYPE_CHOICES = enumerate((
    u'мужская',
    u'женская',
    u'для несовершеннолетних',
    u'СИЗО'
))


class Jail(models.Model):
    place = models.ForeignKey(Place)
    routine = models.IntegerField(choices=ROUTINE_CHOICES)
    typec = models.IntegerField(choices=TYPE_CHOICES)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % (self.name)

    class Meta:
        ordering = ['name']
        verbose_name = u'Исправительное заведение'


JAIL_CHOICES = ((j.id, j.name) for j in Jail.objects.all())


class Imprisonment(models.Model):
    user = models.ForeignKey(User)
    state = models.ForeignKey('imprisonment.State', null=True, blank=True)
    region = ChainedForeignKey('imprisonment.Region', null=True, blank=True,
                               chained_field="state", chained_model_field="state",
                               show_all=False, auto_choose=True)
    place = ChainedForeignKey('imprisonment.Place', null=True, blank=True,
                              chained_field="region", chained_model_field="region",
                              show_all=False, auto_choose=True)
    jail = ChainedForeignKey(Jail, null=True, blank=True,
                             chained_field="place", chained_model_field="place",
                             show_all=False, auto_choose=True)
    begindate = models.DateField()
    enddate = models.DateField()

    def __unicode__(self):
        return u'%s %s %s' % (self.jail, self.begindate, self.enddate)


class Friendship(models.Model):
    from_friend = models.ForeignKey(User, related_name='friend_set')
    to_friend = models.ForeignKey(User, related_name='to_friend_set')

    def __unicode__(self):
        return u'%s, %s' % (
            self.from_friend.username,
            self.to_friend.username
        )

    class Meta:
        unique_together = (('to_friend', 'from_friend'), )


class Invitation(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField()
    code = models.CharField(max_length=20)
    sender = models.ForeignKey(User)

    def __unicode__(self):
        return u'%s, %s' % (self.sender.username, self.email)

    def send(self):
        subject = u'Приглашение в сеть Односидельцы'
        link = 'http://%s/friend/accept/%s/' % (
            settings.SITE_HOST,
            self.code
        )
        template = get_template('invitation_email.txt')
        context = Context({
            'name': self.name,
            'link': link,
            'sender': self.sender.username,
        })
        message = template.render(context)
        send_mail(
            subject, message,
            settings.DEFAULT_FROM_EMAIL, [self.email]
        )

