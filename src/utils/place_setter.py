from random import randrange
import sys
import os


sys.path.append('/Users/sima/git_repo/odnosidelci/wsgi')
os.environ['DJANGO_SETTINGS_MODULE'] = 'openshift.settings'

from userprofiles.models import UserProfile, Place

users = UserProfile.objects

for user in users:
    if user.is_fake:
        place_id = randrange(100000)
        place = Place.objects.filter(id=place_id)
        if len(place) > 0:
            district = place[0].district
            region = district.region
            state = region.state
            user.state_id = state.id
            user.region_id = region.id
            user.district_id = district.id
            user.place_id = place_id
            print 'update userprofiles_userprofile set state_id=%(state_id)d, \
                                                       region_id=%(region_id)d, \
                                                       district_id=%(district_id)d, \
                                                       place_id=%(place_id)d \
                                                       where user_id=%(user_id)d;' \
                  % {'state_id': state.id, 'region_id': region.id, 'district_id': district.id, 'place_id': place_id, 'user_id': user.id}
