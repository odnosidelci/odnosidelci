from haystack import indexes
from userprofiles.models import UserProfile


class UserProfileIndex(indexes.SearchIndex, indexes.Indexable):

    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return UserProfile