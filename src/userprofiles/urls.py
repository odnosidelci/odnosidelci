from django.conf.urls.defaults import patterns, include, url
from userprofiles.views import *

urlpatterns = patterns('',
                       url(r'^profile/$', profile_edit),
)
