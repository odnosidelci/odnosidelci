# -*- coding: utf-8 -*-

from django.contrib import admin
from imprisonment.models import State, Region, Place, Jail, Imprisonment, Friendship, Invitation

# page 128 "Django подробное руководство"


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'state')


class PlaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'region')


class JailAdmin(admin.ModelAdmin):
    list_display = ('name', 'place', 'routine', 'typec')


class ImprisonmentAdmin(admin.ModelAdmin):
    list_display = ('user', 'jail', 'begindate', 'enddate')


# page 135 "Django подробное руководство"


admin.site.register(State)
admin.site.register(Region, RegionAdmin)
admin.site.register(Place, PlaceAdmin)
admin.site.register(Jail, JailAdmin)
admin.site.register(Imprisonment, ImprisonmentAdmin)
admin.site.register(Friendship)
admin.site.register(Invitation)

