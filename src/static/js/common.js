(function($) {
    $.fn.stylingform = function(){
        $.each($("div.checkbox .check"),function(){
                if($("input", this).is(':checked')){
                    $(this).closest('.checkbox').find('.check').addClass("chckd");
                }
            }
        );
        $('div.checkbox .check', this).click(function(){
            if($('input', this).is(':checked')){
                $('input', this).attr("checked", false);
            }else{
                $('input', this).attr("checked", true);
            }
            $(this).closest('.checkbox').find('.check').toggleClass('chckd');
        });
        $('div.a .btn', this).click(function(){
            if($(this).closest('.a').find('input').attr('value') == 'off'){
                $(this).closest('.a').find('input').attr('value', 'on');
                $(this).closest('.a').find('.ind').html('активно');
            }else{
                $(this).closest('.a').find('input').attr('value', 'off');
                $(this).closest('.a').find('.ind').html('не активно');
            }
        });
    }
    $(document).ready(function(){
        $('.modal .close').click(function(){
           $(this).closest('.modal').fadeOut('fast');
        });
        $('.give-present').click(function(){
            $('.present').closest('.modal').fadeIn('fast');
        });
        $('.noplace').click(function(){
            $('.add-place').closest('.modal').fadeIn('fast');
        });
        $('.addfriend').click(function(){
            $('.add-place').closest('.add-friend').fadeIn('fast');
        });
        $('div.select .show', this).click(function(){
            $(this).closest('.select').find('.options').fadeIn('fast');
            $(this).closest('.select').find('.options .sh').click(function(){
                $(this).closest('.options').fadeOut('fast');
            });
            $(this).closest('.select').find('.options .option').click(function(){
                $(this).closest('.select').find('input').attr('value', $(this).attr('data-value'));
                $(this).closest('.select').find('.show').html($(this).html());
                $(this).closest('.options').fadeOut('fast');
            });
        });
        //$('div.options', this).mouseleave(function(){
        //    $(this).fadeOut('fast');
        //});
    });
})(jQuery);