# -*- coding: utf-8 -*-

import re
import datetime

from django import forms
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy as _

from imprisonment.models import *


# p.168 "Django. Подробное.руководство" - Изменение способа отображения полей.

class CalendarTigraWidget(forms.TextInput):
    # https://docs.djangoproject.com/en/dev/topics/forms/media/
    class Media:
        js = ( '/site_media/tcal.js',)
        css = {
            'all': ( '/site_media/tcal.css',)
        }

    def __init__(self, attrs={}):
        super(CalendarTigraWidget, self).__init__(attrs={'class': 'tcal', 'size': '10', 'name': 'date'})


# EXTRA_CHOICES = [
# ('0', u'--- В этом списке такого учреждения нет ---'),
# ]


class ImprisonmentEditForm(forms.ModelForm):
    def __init__(self, *args, **kw):
        super(ImprisonmentEditForm, self).__init__(*args, **kw)
        self.fields['begindate'].widget = CalendarTigraWidget()
        self.fields['begindate'].label = u'Начало'
        self.fields['begindate'].required = True
        self.fields['enddate'].widget = CalendarTigraWidget()
        self.fields['enddate'].label = u'Конец'
        self.fields['enddate'].required = True
        self.fields['jail'].label = u'Учреждение'
        self.fields['jail'].required = True
        self.fields['state'].label = u'Страна'
        self.fields['state'].required = True
        self.fields['region'].label = u'Регион'
        self.fields['region'].required = True
        self.fields['place'].label = u'Населенный пункт'
        self.fields['place'].required = True

    class Meta:
        model = Imprisonment
        exclude = ['user']


class ImprisonmentSaveForm(forms.Form):
    # http://stackoverflow.com/questions/3700445/add-additional-options-to-django-form-select-widget
    jail = forms.ChoiceField(label=u'Место отсидки',
                             choices=(),
                             widget=forms.Select(attrs={'class': 'selector'}))

    # https://docs.djangoproject.com/en/dev/ref/settings/#date-format
    begindate = forms.DateField(label=u'Дата начала срока', widget=CalendarTigraWidget)
    enddate = forms.DateField(label=u'Дата окончания срока', widget=CalendarTigraWidget)

    # share = forms.BooleanField( label=u'Share on the main page', required=False )

    def __init__(self, *args, **kwargs):
        super(ImprisonmentSaveForm, self).__init__(*args, **kwargs)
        self.message = ""
        #pprint.pprint(args)    
        # (<QueryDict: {u'enddate': [u'2012-04-02'],..., u'jail': [u'43'], u'begindate': [u'2012-04-01']}>, 
        #  u'tester'
        # )
        if len(args) == 2:
            self.username = args[1]

        choices = [('', u'--- Выберите учреждение из этого списка ---')]
        choices.extend([(j.id, j.name) for j in Jail.objects.all()])
        #choices.extend(EXTRA_CHOICES)
        self.fields['jail'].choices = choices


    def is_overlap(self, username):
        user = get_object_or_404(User, username=username)
        query_set = user.imprisonment_set.order_by('-begindate')
        for q in query_set:
            if self.cleaned_data['begindate'] >= q.begindate and \
                            self.cleaned_data['begindate'] <= q.enddate or \
                                    self.cleaned_data['enddate'] >= q.begindate and \
                                    self.cleaned_data['enddate'] <= q.enddate:
                self.message = u"%s с %s по %s" % ( q.jail, q.begindate, q.enddate)
                return True

            return False


    # p.169 "Django. Подробное.руководство" - Добавление собственных правил проверки.

    def clean_enddate(self):
        if self.cleaned_data['enddate'] <= self.cleaned_data['begindate']:
            raise forms.ValidationError(u'Дата начала срока должна предшествовать дате окончания срока.')

        if self.is_overlap(self.username):
            raise forms.ValidationError(
                u'Этот срок перекрывается со сроком имеющейся уже ходки: "%s"' % self.message
            )
        if self.cleaned_data['enddate'] > datetime.date.today():
            raise forms.ValidationError(u'Дата окончания завершенного срока не должна быть в будущем.')

        return self.cleaned_data['enddate']


class RegistrationForm(forms.Form):
    username = forms.CharField(label=u'Имя пользователя', max_length=30)
    email = forms.EmailField(label=u'Email')
    password1 = forms.CharField(
        label=u'Пароль',
        widget=forms.PasswordInput()
    )
    password2 = forms.CharField(
        label=u'Пароль (еще раз)',
        widget=forms.PasswordInput()
    )

    def clean_password2(self):
        if 'password1' in self.cleaned_data:
            password1 = self.cleaned_data['password1']
            password2 = self.cleaned_data['password2']
            if password1 == password2:
                return password2
        raise forms.ValidationError(u'Пароли не совпадают.')

    def clean_username(self):
        username = self.cleaned_data['username']
        if not re.search(r'^\w+$', username):
            raise forms.ValidationError(
                u'Имя пользователя может содержать только буквы латинского алфавита, цифры и символ подчеркивания.')
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(u'Это имя пользователя уже занято.')


class FriendInviteForm(forms.Form):
    name = forms.CharField(label=_(u"Имя друга"))
    email = forms.EmailField(label=_(u"Email друга"))
