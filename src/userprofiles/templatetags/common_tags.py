from django import template
from django.contrib.sites.models import Site

register = template.Library()

@register.simple_tag
def get_canonical(request):
    ret = ''
    current_site = Site.objects.get_current()
    path = request.get_full_path()
    if not path.startswith('/accounts/') and not path.startswith('/meet/'):
        url = 'http://' + current_site.domain + path
        ret = '<link rel="canonical" href="' + url + '"/>'
    return ret