# -*- coding: utf-8 -*-

from django.contrib import admin 
from userprofiles.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_filter = ('is_fake', 'sex', 'is_vip',)
    search_fields = ('user__username', 'nickname',)

try:
    admin.site.unregister(UserProfile)
except:
    pass

admin.site.register(UserProfile, UserProfileAdmin)