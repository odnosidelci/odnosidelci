# -*- coding: utf-8 -*-

import datetime

from django.db import models
from django.contrib.auth.models import User

from odnosidelci.settings.common import SITE_HOST
from userena.models import UserenaBaseProfile
from smart_selects.db_fields import ChainedForeignKey


SEX_CHOICES = (('u', u'Неважно'), ('m', 'Мужской'), ('f', 'Женский'))


class State(models.Model):
    name = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Страна"


class Region(models.Model):
    state = models.ForeignKey(State)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Регион"


class District(models.Model):
    region = models.ForeignKey(Region)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Район"


class Place(models.Model):
    district = models.ForeignKey(District)
    name = models.CharField(max_length=60)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        ordering = ['name']
        verbose_name = "Населенный пункт"


class UserProfile(UserenaBaseProfile):
    second_name = models.CharField("Отчество", max_length=50, null=True, blank=True)
    nickname = models.CharField("Кличка", max_length=50, null=True, blank=True)
    mood = models.CharField("Обо мне", max_length=250, null=True, blank=True)
    birth_date = models.DateField("Дата рождения", null=True, blank=True)
    sex = models.CharField("Пол", max_length=1, default='u', choices=SEX_CHOICES)
    state = models.ForeignKey(State, null=True, blank=True)
    region = ChainedForeignKey(Region, null=True, blank=True,
                               chained_field="state", chained_model_field="state",
                               show_all=False, auto_choose=True)
    district = ChainedForeignKey(District, null=True, blank=True,
                                 chained_field="region", chained_model_field="region",
                                 show_all=False, auto_choose=True)
    place = ChainedForeignKey(Place, null=True, blank=True,
                              chained_field="district", chained_model_field="district",
                              show_all=False, auto_choose=True)
    #avatar = models.ImageField(upload_to="avatars/%Y/%m/%d",
    #    height_field='avatar_height', width_field='avatar_width',
    #    null=True, blank=True)
    #avatar_width = models.IntegerField(default=0)
    #avatar_height = models.IntegerField(default=0)
    user = models.ForeignKey(User, unique=True)
    last_operation = models.DateTimeField(default=datetime.datetime.now)
    is_vip = models.BooleanField(default=False)
    no_ad = models.BooleanField(default=False)
    is_hidden = models.BooleanField(default=False)
    is_invisible = models.BooleanField(default=False)
    hide_xodkas = models.BooleanField(default=False)
    rating = models.IntegerField(default=0)
    is_fake = models.BooleanField(default=False)

    # def get_full_name(self):
    #     if self.user.last_name or self.user.first_name or self.second_name:
    #         if self.second_name:
    #             return "%s %s %s" % (self.user.last_name, self.user.first_name,
    #                                  self.second_name)
    #         else:
    #             return "%s %s" % (self.user.last_name, self.user.first_name)
    #     else:
    #         return "Аноним"

    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'

    def get_full_name(self):
        if self.user.last_name or self.user.first_name:
            return "%s %s" % (self.user.first_name, self.user.last_name)
        else:
            return "Аноним"

    def get_place(self):
        if self.place:
            return self.place
        else:
            return "Место жительства не указано"

    def get_mood(self):
        if self.mood:
            return self.mood
        else:
            return ""

    def get_absolute_url(self):
        return "http://%s/accounts/%s/" % (SITE_HOST, self.user.username)

    def __unicode__(self):
        return self.user.username

    def friends(self):
        return self.user.friend_set.all()


User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class UserSearch(models.Model):
    sex = models.CharField("Пол", max_length=1, default='u', choices=SEX_CHOICES)
    state = models.ForeignKey(State, null=True, blank=True)
    region = ChainedForeignKey(Region, null=True, blank=True,
                               chained_field="state", chained_model_field="state",
                               show_all=False, auto_choose=True)
    district = ChainedForeignKey(District, null=True, blank=True,
                                 chained_field="region", chained_model_field="region",
                                 show_all=False, auto_choose=True)
    place = ChainedForeignKey(Place, null=True, blank=True,
                              chained_field="district", chained_model_field="district",
                              show_all=False, auto_choose=True)
