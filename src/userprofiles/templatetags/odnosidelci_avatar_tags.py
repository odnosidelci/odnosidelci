from django.conf import settings
from django import template
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from avatar.settings import (AVATAR_DEFAULT_URL, AVATAR_DEFAULT_SIZE)
from avatar.util import get_primary_avatar

register = template.Library()


@register.simple_tag
def avatar_url(user, size=AVATAR_DEFAULT_SIZE):
    avatar = get_primary_avatar(user, size=size)
    if avatar:
        return avatar.avatar_url(size)
    else:
        return get_default_avatar_url(user)


@register.simple_tag
def odnosidelci_avatar(user, size=AVATAR_DEFAULT_SIZE):
    if not isinstance(user, User):
        try:
            user = User.objects.get(username=user)
            alt = unicode(user)
            url = avatar_url(user, size)
        except User.DoesNotExist:
            url = get_default_avatar_url(user)
            alt = _("Default Avatar")
    else:
        alt = unicode(user)
        url = avatar_url(user, size)
    return """<img src="%s" alt="%s" width="%s" height="%s" />""" % (url, alt,
                                                                     size, size)


def get_default_avatar_url(user):
    avatar = AVATAR_DEFAULT_URL
    if user.get_profile().sex == 'f':
        avatar = settings.FEMALE_AVATAR_DEFAULT_URL
    base_url = getattr(settings, 'STATIC_URL', None)
    if not base_url:
        base_url = getattr(settings, 'MEDIA_URL', '')
    # Don't use base_url if the default avatar url starts with http:// of https://
    if avatar.startswith('http://') or avatar.startswith('https://'):
        return avatar
    # We'll be nice and make sure there are no duplicated forward slashes
    ends = base_url.endswith('/')
    begins = avatar.startswith('/')
    if ends and begins:
        base_url = base_url[:-1]
    elif not ends and not begins:
        return '%s/%s' % (base_url, avatar)
    return '%s%s' % (base_url, avatar)