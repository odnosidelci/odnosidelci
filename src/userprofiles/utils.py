import datetime
import time
from odnosidelci.settings import common
from redis import StrictRedis


class Limits(object):
    _view_limit_key = "odnosidelci:views:%(user_id)s"
    _message_limit_key = "odnosidelci:messages:%(user_id)s"

    def __init__(self):
        self._redis_conn = StrictRedis(host=common.REDIS_HOST, port=common.REDIS_PORT)

    def view_limit(self, user, profile_id):
        key = self._view_limit_key % {'user_id': user.id}
        if not self._redis_conn.exists(key):
            self._redis_conn.sadd(key, profile_id)
            self._redis_conn.expireat(key, Limits.new_day_start())
            return True
        else:
            if self._redis_conn.sismember(key, profile_id) or self._redis_conn.scard(key) < common.PROFILE_VIEW_LIMIT:
                self._redis_conn.sadd(key, profile_id)
                return True
            else:
                return False

    def message_limit(self, user, profile_id):
        key = self._message_limit_key % {'user_id': user.id}
        if not self._redis_conn.exists(key):
            self._redis_conn.sadd(key, profile_id)
            self._redis_conn.expireat(key, Limits.new_day_start())
            return True
        else:
            if self._redis_conn.sismember(key, profile_id) or self._redis_conn.scard(key) < common.MESSAGE_VIEW_WRITE:
                self._redis_conn.sadd(key, profile_id)
                return True
            else:
                return False

    @staticmethod
    def new_day_start():
        now = datetime.datetime.now()
        now = now.replace(day=now.day + 1, hour=0, minute=0, second=0)
        return int(time.mktime(now.timetuple()))
