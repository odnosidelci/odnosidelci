# -*- coding: utf-8 -*-

import smtplib

from django.shortcuts import render_to_response
from django.contrib.auth import logout
from django.http import HttpResponseRedirect, Http404
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, InvalidPage
from django.db.models import Q
from django.contrib import messages
from django.shortcuts import redirect

from imprisonment.forms import *


ITEMS_PER_PAGE = 5


def main_page(request):
    values = request.META.items()
    values.sort()
    # locals() возвращает словарь, отображающий имена всех  
    # локальных переменных на их значения, где под локальными понимаются  
    # переменные, определенные в текущей области видимости. 
    # page 87 "Django подробное руководство"
    variables = RequestContext(request, locals())

    return render_to_response('start.html', variables)


def new_main_page(request):
    values = request.META.items()
    values.sort()
    # locals() возвращает словарь, отображающий имена всех  
    # локальных переменных на их значения, где под локальными понимаются  
    # переменные, определенные в текущей области видимости. 
    # page 87 "Django подробное руководство"
    variables = RequestContext(request, locals())
    if request.user.is_authenticated():
        return redirect('accounts/%s/' % request.user.username)
    return render_to_response('new_start.html', variables)


def logout_page(request):
    logout(request)
    return HttpResponseRedirect('/')


def user_page(request, username):
    if request.method == 'POST':
        form = ImprisonmentEditForm(request.POST, request.user.username)
        if form.is_valid():
            imprisonment = _imprisonment_save(request, form)
            return HttpResponseRedirect('/imprisonment/user/%s/' % request.user.username)

    else:
        form = ImprisonmentEditForm()

    user = get_object_or_404(User, username=username)
    query_set = user.imprisonment_set.order_by('-begindate')
    paginator = Paginator(query_set, ITEMS_PER_PAGE)
    show_delete = False
    if request.user.username == username:
        show_delete = True

    if request.user.is_authenticated():
        is_friend = Friendship.objects.filter(
            from_friend=request.user,
            to_friend=user
        )
    else:
        is_friend = False
    try:
        page_number = int(request.GET['page'])
    except (KeyError, ValueError):
        page_number = 1

    try:
        page = paginator.page(page_number)
    except InvalidPage:
        raise Http404

    imprisonments = page.object_list

    variables = RequestContext(request, {
        'username': username,
        'form': form,
        'imprisonments': imprisonments,
        'show_delete': show_delete,
        'show_edit': username == request.user.username,
        'show_paginator': paginator.num_pages > 1,
        'has_prev': page.has_previous(),
        'has_next': page.has_next(),
        'page': page_number,
        'pages': paginator.num_pages,
        'next_page': page_number + 1,
        'prev_page': page_number - 1,
        'is_friend': is_friend,
    })

    return render_to_response('user_page.html', variables)


def _imprisonment_save(request, form):
    # Get Jail.
    jail = form.cleaned_data['jail']

    # Create or get Imprisonment.
    imprisonment, created = Imprisonment.objects.get_or_create(
        user=request.user,
        jail=jail,
        begindate=form.cleaned_data['begindate'],
        enddate=form.cleaned_data['enddate']
    )
    # Save xodka to database and return it.
    imprisonment.save()
    return imprisonment


@login_required
def imprisonment_odnosid(request, idx):
    if request.method == 'POST':
        form = ImprisonmentEditForm(request.POST, request.user.username)
        if form.is_valid():
            imprisonment = _imprisonment_save(request, form)
            return HttpResponseRedirect('/imprisonment/user/%s/' % request.user.username)

    else:
        form = ImprisonmentEditForm()

    try:
        x = Imprisonment.objects.get(user=request.user, id=idx)
    except Imprisonment.DoesNotExist:
        return HttpResponseRedirect('/imprisonment/user/%s/' % request.user.username)
    # Односидельцы по данной ходке:
    """
             |===========| Ходка
    1. |-------|
    2.            |--------|
    3.       |-----|
    4.  |---------------|
    """
    ods = [y for y in Imprisonment.objects.filter
    (Q(jail=x.jail) & ~Q(user=x.user) & (
        Q(begindate__lte=x.begindate) & Q(enddate__range=(x.begindate, x.enddate)) |
        Q(begindate__range=(x.begindate, x.enddate)) & Q(enddate__gte=x.enddate) |
        Q(begindate__range=(x.begindate, x.enddate)) & Q(enddate__range=(x.begindate, x.enddate)) |
        Q(begindate__lte=x.begindate) & Q(enddate__gte=x.enddate)
    )
    ).order_by('-id')]

    variables = RequestContext(request, {
        'username': request.user.username,
        'imprisonment': x,
        'odnosids': ods,
        'form': form,
    })

    return render_to_response('odnosids_page.html', variables)


@login_required
def imprisonment_delete(request, idx):
    try:
        imprisonment = Imprisonment.objects.get(user=request.user, id=idx)
    except Imprisonment.DoesNotExist:
        pass
    else:
        imprisonment.delete()
    return HttpResponseRedirect('/imprisonment/user/%s/' % request.user.username)


@login_required
def imprisonment_save_page(request):
    if request.method == 'POST':
        # pprint.pprint(request.POST)
        # <QueryDict: {u'enddate': [u'2012-03-10'], ... u'jail': [u'75'], u'begindate': [u'2012-03-08']}>

        form = ImprisonmentEditForm(request.POST, request.user.username)

        if form.is_valid():
            imprisonment = _imprisonment_save(request, form)
            return HttpResponseRedirect('/imprisonment/user/%s/' % request.user.username)

    else:
        form = ImprisonmentEditForm()

    variables = RequestContext(request, {'form': form})
    return render_to_response('imprisonment_save_form.html', variables)


def register_page(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password1'],
                email=form.cleaned_data['email']
            )
            if 'invitation' in request.session:
                # Retrieve the invitation object.
                invitation = Invitation.objects.get(
                    id=request.session['invitation']
                )
                # Create friendship from user to sender.
                friendship = Friendship(
                    from_friend=user,
                    to_friend=invitation.sender
                )
                friendship.save()
                # Create friendship from sender to user.
                friendship = Friendship(
                    from_friend=invitation.sender,
                    to_friend=user
                )
                friendship.save()
                # Delete the invitation from the database and session.
                invitation.delete()
                del request.session['invitation']
            return HttpResponseRedirect('/register/success/')
    else:
        form = RegistrationForm()

    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/register.html', variables)


def friends_page(request, username):
    user = get_object_or_404(User, username=username)
    friends = [friendship.to_friend for friendship in user.friend_set.all()]
    friend_imprisonments = Imprisonment.objects.filter(user__in=friends).order_by('-id')
    variables = RequestContext(request, {
        'username': username,
        'friends': friends,
        'imprisonment': friend_imprisonments[:10],
        'show_tags': True,
        'show_user': True
    })
    return render_to_response('friends_page.html', variables)


@login_required
def friend_add(request, username):
    if username:
        friend = get_object_or_404(User, username=username)
        friendship = Friendship(from_friend=request.user, to_friend=friend)
        try:
            friendship.save()
            messages.add_message(request, messages.INFO,
                                 u'Пользователь %s добавлен в список Ваших друзей.' % friend.username)
        except:
            messages.add_message(request, messages.INFO, u'%s уже Ваш друг.' % friend.username)
        return HttpResponseRedirect('/friends/%s/' % request.user.username)
    else:
        raise Http404


@login_required
def friend_invite(request):
    if request.method == 'POST':
        form = FriendInviteForm(request.POST)
        if form.is_valid():
            invitation = Invitation(
                name=form.cleaned_data['name'],
                email=form.cleaned_data['email'],
                code=User.objects.make_random_password(20),
                sender=request.user
            )
            invitation.save()
            try:
                invitation.send()
                messages.add_message(request, messages.INFO,
                                     u'Приглашение отправлено на адрес %(email)s.' % {'email': invitation.email}
                )
            except smtplib.SMTPException:
                messages.add_message(request, messages.INFO,
                                     u'Произошла ошибка при отправке приглашения.')
            return HttpResponseRedirect('/friend/invite/')
    else:
        form = FriendInviteForm()

    variables = RequestContext(request, {
        'form': form
    })
    return render_to_response('friend_invite.html', variables)


def friend_accept(request, code):
    invitation = get_object_or_404(Invitation, code__exact=code)
    request.session['invitation'] = invitation.id
    return HttpResponseRedirect('/register/')
