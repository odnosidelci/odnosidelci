# -*- coding: utf-8 -*-

import datetime

from django import forms

from imprisonment.forms import CalendarTigraWidget
from userprofiles.models import UserSearch


# p.168 "Django. Подробное.руководство" - Изменение способа отображения полей.


def years():
    y = datetime.date.today().year
    a = []
    a.append((0, u'неважно'))
    for i in range(y - 80, y - 16):
        a.append((i, str(i)))
    return a


class ProfileEditForm(forms.Form):
    first_name = forms.CharField(label=u'Имя', max_length=50)
    second_name = forms.CharField(label=u'Отчество', max_length=50,
                                  required=False)
    last_name = forms.CharField(label=u'Фамилия', max_length=50, required=False)
    birth_date = forms.DateField(label=u'Дата рождения',
                                 widget=CalendarTigraWidget, required=False)
    state = forms.ChoiceField(label=u'Страна')


class MeetForm(forms.ModelForm):
    birth_year_start = forms.ChoiceField(label=u'Год рождения с', choices=years())
    birth_year_end = forms.ChoiceField(label=u'Год рождения по', choices=years())

    def __init__(self, *args, **kw):
        super(MeetForm, self).__init__(*args, **kw)

        # new_order = self.fields.keyOrder[:-2]
        # new_order.insert(1, 'birth_year_start')
        #new_order.insert(2, 'birth_year_end')
        #self.fields.keyOrder = new_order

        self.fields['state'].label = u'Страна'
        self.fields['region'].label = u'Регион'
        self.fields['district'].label = u'Район'
        self.fields['place'].label = u'Населенный пункт'

    class Meta:
        model = UserSearch

    def save(self, force_insert=False, force_update=False, commit=True):
        pass