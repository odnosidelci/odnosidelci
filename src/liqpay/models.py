# -*- coding: utf-8 -*-

import base64
import hashlib
# from xml.dom.minidom import *
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from datetime import datetime


class LiqPayIn(models.Model):
    """
    """
    STATUS_CHOICES = (
        (0, _('Registered')),
        (100, _('Initiated')),
        (200, _('Aborted')),
        (400, _('Finished successfully')),
    )
    payment_from = models.ForeignKey(User, blank=True, null=True, related_name="payment_from")
    vip_account_to = models.ForeignKey(User, blank=True, null=True, related_name="vip_account_to")
    present_to = models.ForeignKey(User, blank=True, null=True, related_name="present_to")
    #present = models.ForeignKey(Present, blank=True, null=True)

    created = models.DateField(_("created"), default=datetime.now)
    status = models.IntegerField(_("status"), max_length=1,
                                 choices=STATUS_CHOICES, default=0)
    amount = models.DecimalField(_("amount"), decimal_places=2, max_digits=10)
    currency = models.CharField(_("currency"), max_length=3, default="UAH")
    sender = models.CharField(_("MSISDN of sender"), max_length=15, blank=True,
                              null=True)
    operation_xml = models.TextField(_("operation XML"), blank=True, null=True)
    signature = models.TextField(_("signature"), blank=True, null=True)
    request_in_xml = models.TextField(_("LiqPAY incomming request XML"),
                                      blank=True, null=True)

    def __unicode__(self):
        return "IN%d" % self.pk

    def generate_form_fields(self):
        if self.status == 0:
            xml = """<request>      
     <version>1.2</version>
     <merchant_id>%s</merchant_id>
     <result_url>http://www.odnosidelci.com/</result_url>
     <server_url>http://www.odnosidelci.com/liqpay/pay_in/</server_url>
     <order_id>%d</order_id>
     <amount>%.2f</amount>
     <currency>%s</currency>
     <description>In%d</description>
     <default_phone></default_phone>
     <pay_way>card,liqpay,delayed</pay_way>
     <goods_id>1234</goods_id>
</request>""" % (local_settings.LIQPAY_ID, self.id,
                 self.amount, self.currency.iso_code,
                 self.id)
            self.operation_xml = base64.b64encode(xml)
            signature_hash = hashlib.sha1()
            signature_hash.update(local_settings.LIQPAY_GENERAL_SIG + xml + local_settings.LIQPAY_GENERAL_SIG)
            self.signature = base64.b64encode(signature_hash.digest())
            self.save()
            return True
        else:
            return False

