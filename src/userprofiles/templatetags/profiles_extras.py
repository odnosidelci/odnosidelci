# -*- coding: utf-8

from django import template
from userprofiles.models import UserProfile
from userprofiles.forms import MeetForm

register = template.Library()


@register.simple_tag
def count_users():
    return UserProfile.objects.get_visible_profiles().count()


@register.simple_tag
def get_meet_form():
    return MeetForm().as_p()

@register.assignment_tag(takes_context=True)
def is_friends(context, main_user, user, var):
    pass
    # context[var] = False
    # if len(main_user.friend_set.filter(from_friend=main_user, to_friend=user)) == 0:
    #     context[var] = False
    #     print context
    # else:
    #     context[var] = True
    #     print context
