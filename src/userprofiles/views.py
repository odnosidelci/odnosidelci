# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
import datetime
from avatar.models import Avatar

from userprofiles.models import UserProfile
from userprofiles.forms import ProfileEditForm, MeetForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from random import shuffle

ITEMS_PER_PAGE = 10


@login_required
def profile_edit(request):
    if request.method == 'GET':
        user = request.user

        data = {'first_name': user.first_name, 'last_name': user.last_name,
                'second_name': user.profile.second_name,
                'birth_date': user.profile.birth_date,
        }
        form = ProfileEditForm(data)
    else:
        form = ProfileEditForm(request.POST)
        if form.is_valid():
            user = request.user
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            profile = user.profile
            profile.second_name = form.cleaned_data['second_name']
            profile.birth_date = form.cleaned_data['birth_date']
            user.save()
            profile.save()

    variables = RequestContext(request, {'form': form, 'u': request.user})
    return render_to_response('profile-settings.html', variables)


@login_required
def meet(request):
    people = None
    page = None
    if request.method == 'GET':
        form = MeetForm()
        if request.session.get('meet_search_params', None) is not None:
            params = request.session['meet_search_params']
            page = request.GET.get('page')
            people = meet_search(user_id=request.user.id,
                                 sex=params['sex'],
                                 state=params['state'],
                                 region=params['region'],
                                 district=params['district'],
                                 place=params['place'],
                                 birth_year_start=int(params['birth_year_start']),
                                 birth_year_end=int(params['birth_year_end']))
    else:
        form = MeetForm(request.POST)
        if form.is_valid():
            people = meet_search(user_id=request.user.id,
                                 sex=form.cleaned_data['sex'],
                                 state=form.cleaned_data['state'],
                                 region=form.cleaned_data['region'],
                                 district=form.cleaned_data['district'],
                                 place=form.cleaned_data['place'],
                                 birth_year_start=int(form.cleaned_data['birth_year_start']),
                                 birth_year_end=int(form.cleaned_data['birth_year_end']))

            request.session['meet_search_params'] = form.cleaned_data

    if people is not None:
        paginator = Paginator(people, ITEMS_PER_PAGE)
        try:
            people = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            people = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            # people = paginator.page(paginator.num_pages)
            people = None
    variables = RequestContext(request, {'form': form, 'people': people})
    return render_to_response('meet_base.html', variables)


def carousel(request):
    avatars = list(Avatar.objects.filter(primary=True)[:60])
    shuffle(avatars)
    avatars_dict = {}
    avatar_group_list = []
    counter = 0
    group = 1
    for avatar in avatars:
        if counter != 0 and counter % 6 == 0:
            avatars_dict[group] = avatar_group_list
            group += 1
            avatar_group_list = []
        avatar_group_list.append(avatar)
        counter += 1
    avatars_dict[group] = avatar_group_list
    return render_to_response('carousel_data.html', {'avatars_dict': avatars_dict})


def meet_search(user_id, sex, state, region, district, place, birth_year_start, birth_year_end):
    people = UserProfile.objects.all()
    if birth_year_start:
        people = people.filter(birth_date__gte=datetime.date(birth_year_start, 1, 1))
    if birth_year_end:
        people = people.filter(birth_date__lte=datetime.date(birth_year_end, 12, 31))
    if sex != 'u':
        people = people.filter(sex=sex)
    if state:
        people = people.filter(state=state)
    if region:
        people = people.filter(region=region)
    if district:
        people = people.filter(district=district)
    if place:
        people = people.filter(place=place)
    return people.exclude(user_id=user_id)