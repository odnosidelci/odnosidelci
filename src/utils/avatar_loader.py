from random import randrange
from os import listdir
from os.path import isfile, join
import sys
import os
from PIL import Image
import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile


sys.path.append('/Users/sima/git_repo/odnosidelci/wsgi')
os.environ['DJANGO_SETTINGS_MODULE'] = 'openshift.settings'

from django.contrib.auth.models import User
from avatar.models import Avatar
from userprofiles.models import UserProfile

AVATAR_ROOT = '/Users/sima/test_ava'
SEX = 'm'


class UserDTO(object):
    def __init__(self, user, f):
        self.user = user
        self.f = f

user_dict = {}

for f in listdir(AVATAR_ROOT):
    if isfile(join(AVATAR_ROOT, f)):
        user = []
        while len(user) == 0:
            random_id = randrange(100000)
            user = UserProfile.objects.filter(user_id=random_id)
            if len(user) > 0 and user[0].user.id not in user_dict and user[0].sex == SEX and user[0].is_fake:
                user_dto = UserDTO(user=user[0], f=f)
                user_dict[user[0].user.id] = user_dto
            else:
                user = []

for key in user_dict:
    print key
    avatar = Avatar(user=user_dict[key].user, primary=True)
    thumb = Image.open(AVATAR_ROOT + '/' + user_dict[key].f)
    thumb_io = StringIO.StringIO()
    thumb.save(thumb_io, format='JPEG')
    thumb_file = InMemoryUploadedFile(thumb_io, None, user_dict[key].f, 'image/jpeg', thumb_io.len, None)

    avatar.avatar.save(thumb_file.name, thumb_file)
    avatar.save()