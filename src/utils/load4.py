# -*- coding: utf-8 -*-

# Last edited: 24 Feb. 2012
# V.V.

import csv

from django.core.management import setup_environ

from odnosidelci.settings import common

setup_environ(common)

from imprisonment.models import State, Region, Place, Jail
from django.utils.encoding import *


file_csv = "ubr_koi8-u.csv"

# http://proft.me/2009/04/16/operator-switch-v-python/
def switch_routine(case):
    return {
        u'общий': 0,
        u'усиленный': 1,
        u'строгий': 2,
        u'особый': 3,
        u'поселение': 4,
        u'воспитательная': 5,
        u'больница': 6,
        u'тюрьма': 7,
        u'ИК': 8,
        u'СИЗО': 9,
        u'ОИК': 10
    }.get(case, 111)


def switch_typec(case):
    return {
        u'мужская': 0,
        u'женская': 1,
        u'для несовершеннолетних': 2,
        u'СИЗО': 3
    }.get(case, 111)


def read_csv(tfile="default.csv"):
    f = open(tfile, 'rt')
    reader = csv.reader(f)
    for row in reader:
        state = smart_unicode(row[6], encoding='utf-8', strings_only=False, errors='strict')
        region = smart_unicode(row[5], encoding='utf-8', strings_only=False, errors='strict')
        place = smart_unicode(row[4], encoding='utf-8', strings_only=False, errors='strict')
        typec = smart_unicode(row[3], encoding='utf-8', strings_only=False, errors='strict')
        routine = smart_unicode(row[2], encoding='utf-8', strings_only=False, errors='strict')
        if row[2] == u'СИЗО':
            typec = row[2]
        jail = smart_unicode(row[1], encoding='utf-8', strings_only=False, errors='strict')
        print "%s %s %s(%d '%d')" % (jail, state, region, switch_routine(routine), switch_typec(typec))

        # --- State ---
        sobj, created = State.objects.get_or_create(name=state)

        # --- Region ---
        robj, created = Region.objects.get_or_create(name=region, state=sobj)

        # --- Place ---
        pobj, created = Place.objects.get_or_create(name=place, region=robj)

        # --- Jail ---
        jobj, created = Jail.objects.get_or_create(name=jail, place=pobj,
                                                   routine=switch_routine(routine), typec=switch_typec(typec))


read_csv(file_csv)

