# -*- coding: utf-8 -*-
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from django.contrib.auth.models import User

from django.test import TestCase
from userprofiles.utils import Limits


class SimpleTest(TestCase):
    def test_limits(self):
        u = User()
        u.id = 1
        limits = Limits()
        limits.view_limit(u, 10)
        for i in range(1, 20):
            limits.view_limit(u, i)
        self.assertTrue(limits.view_limit(u, 20))
        self.assertFalse(limits.view_limit(u, 21))

        limits.message_limit(u, 9)
        for i in range(1, 10):
            limits.message_limit(u, i)
        self.assertTrue(limits.message_limit(u, 10))
        self.assertFalse(limits.message_limit(u, 11))