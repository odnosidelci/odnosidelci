from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import direct_to_template

# Uncomment the next two lines to enable the admin:
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       # Examples:
                       url(r'^$', 'imprisonment.views.new_main_page'),

                       # Session management
                       url(r'^login/$', 'django.contrib.auth.views.login'),
                       url(r'^logout/$', 'imprisonment.views.logout_page'),
                       url(r'^register/$', 'imprisonment.views.register_page'),
                       url(r'^register/success/$', direct_to_template,
                           {'template': 'registration/register_success.html'}),

                       # Friends
                       url(r'^friend/add/(\w+)/$', 'imprisonment.views.friend_add'),
                       url(r'^friend/invite/$', 'imprisonment.views.friend_invite'),
                       url(r'^friend/accept/(\w+)/$', 'imprisonment.views.friend_accept'),
                       url(r'^friends/(\w+)/$', 'imprisonment.views.friends_page'),
                       url(r'^meet/$', 'userprofiles.views.meet'),
                       url(r'^meet/carousel-data/$', 'userprofiles.views.carousel'),

                       # i18n
                       url(r'^i18n/', include('django.conf.urls.i18n')),

                       # Imprisonments
                       url(r'^imprisonment/', include('imprisonment.urls')),

                       # payment
                       url(r'^liqpay/', include('liqpay.urls')),

                       # Profile
                       url(r'^avatar/', include('avatar.urls')),
                       url(r'^accounts/', include('userena.urls')),
                       url(r'^umessages/', include('userena.contrib.umessages.urls')),

                       # Chaining
                       url(r'^chaining/', include('smart_selects.urls')),
                       # url(r'^accounts/', include('openshift.userprofile.urls')),

                       # url(r'^forum/', include('djangobb_forum.urls', namespace='djangobb')),
                       (r'^forum/', include('forum.urls')),
                       url(r'^admin/', include(admin.site.urls)),

                       # haystack index
                       url(r'^search/', include('haystack.urls')),

                       url(r'^robots\.txt$', direct_to_template,
                           {'template': 'robots.txt', 'mimetype': 'text/plain'}),
)
